.. image:: https://img.shields.io/pypi/v/jaraco.itertools.svg
   :target: https://pypi.org/project/jaraco.itertools

.. image:: https://img.shields.io/pypi/pyversions/jaraco.itertools.svg

.. image:: https://github.com/jaraco/jaraco.itertools/workflows/tests/badge.svg
   :target: https://github.com/jaraco/jaraco.itertools/actions?query=workflow%3A%22tests%22
   :alt: tests

.. image:: https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/charliermarsh/ruff/main/assets/badge/v2.json
    :target: https://github.com/astral-sh/ruff
    :alt: Ruff

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style: Black

.. image:: https://readthedocs.org/projects/jaracoitertools/badge/?version=latest
   :target: https://jaracoitertools.readthedocs.io/en/latest/?badge=latest

.. image:: https://img.shields.io/badge/skeleton-2023-informational
   :target: https://blog.jaraco.com/skeleton
