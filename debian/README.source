This package is maintained with git-buildpackage(1). It follows DEP-14 for
branch naming (e.g. using debian/master for the current version in Debian
unstable due Debian Python team policy).

It uses pristine-tar(1) to store enough information in git to generate bit
identical tarballs when building the package without having downloaded an
upstream tarball first.

When working with patches it is recommended to use "gbp pq import" to import
the patches, modify the source and then use "gbp pq export --commit" to commit
the modifications.

The changelog is generated using "gbp dch" so if you submit any changes don't
bother to add changelog entries but rather provide a nice git commit message
that can then end up in the changelog.

It is recommended to build the package with pbuilder using:

    gbp buildpackage --git-pbuilder

For information on how to set up a pbuilder environment see the git-pbuilder(1)
manpage. In short:

    DIST=sid git-pbuilder create
    gbp clone https://salsa.debian.org/python-team/packages/jaraco.itertools.git
    cd jaraco.itertools
    gbp buildpackage --git-pbuilder


Library documentation
=====================

The upstream source comes with sphinx based documentation, but the configuration
is depending on some non packaged stuff which makes it impossible to also
build a package python-jaraco.itertools-doc.
It mainly depending on a Sphinx extension jaraco.packaging.sphinx and rst.linker.

Besides that especially debian/{control,rules} has some preparations included if
someone wants to jump in an want to work on a *doc package.

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sun, 25 Jun 2023 08:08:00 +0200
